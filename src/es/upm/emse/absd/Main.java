package es.upm.emse.absd;

import es.upm.emse.absd.agents.AdventurerAgent;
import es.upm.emse.absd.agents.FairyAgent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.StaleProxyException;

import static es.upm.emse.absd.Utils.ANSI_WHITE;

/**
 * @author      Jose Maria Barambones <j.barambones@upm.es>
 * @version     1.0.2
 *
 * Main Class.
 */
public class Main {

    private static jade.wrapper.AgentContainer cc;

    public static final boolean log = false;

    // Executed from Singletons.
    private static void loadBoot(){

        // Get a hold on JADE runtime
        jade.core.Runtime rt = jade.core.Runtime.instance();

        // Exit the JVM when there are no more containers around
        rt.setCloseVM(true);
        System.out.println("Runtime created");

        // Create a default profile
        Profile profile = new ProfileImpl(null, 1200, null);
        //profile.setParameter("verbosity","5");
        System.out.println("Profile created");

        System.out.println("Launching a whole in-process platform..."+profile);


        try {
            cc = rt.createMainContainer(profile);

            System.out.println("Launching the rma agent on the main container ...");
            cc.createNewAgent("rma","jade.tools.rma.rma", new Object[0]).start();

        } catch (StaleProxyException e) {
            System.err.println("Error during boot!!!");
            e.printStackTrace();
            System.exit(1);
        }

        // now set the default Profile to start a container
        ProfileImpl agentContainerProfile = new ProfileImpl(null, 1200, null);
        //System.out.println("Launching the agent container ..."+agentContainerProfile);

        cc = rt.createAgentContainer(agentContainerProfile);
        //System.out.println("Launching the agent container after ..."+agentContainerProfile);
        System.out.println("Agent Container created");

    }

    private static void loadFairy(String tag) {
        try {
            cc.createNewAgent(tag, FairyAgent.class.getName(), new Object[]{"0"}).start();
        } catch (StaleProxyException e) {
            System.err.println("Error creating agent!!!");
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void loadAdventurer(String tag) {
        try {
            cc.createNewAgent(tag, AdventurerAgent.class.getName(), new Object[]{"0"}).start();
        } catch (StaleProxyException e) {
            System.err.println("Error creating agent!!!");
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static void printHelp() {
        System.out.println("""
            -------------------------
            --- JADE Conversation ---
            -------------------------
            Agents:
                + FairyAgent: An agent that try to talk with his/her colleague.
                + AdventurerAgent: An agent that responds when its called by its name.
            Parameters:
            """
        );
    }

    public static void main(String[] args) {

        System.setProperty("java.util.logging.SimpleFormatter.format", ANSI_WHITE + "[%1$tF %1$tT] [%4$-7s] %5$s %n");

        if (args.length != 0) {
            printHelp();
            System.exit(0);
        }

        System.out.println("Starting...");
        loadBoot();
        System.out.println("MAS loaded...");

        loadFairy("Navi");
        //loadFairy("Taya");
        loadAdventurer("Link");

    }

}
