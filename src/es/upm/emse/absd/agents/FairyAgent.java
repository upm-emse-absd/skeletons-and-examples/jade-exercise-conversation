package es.upm.emse.absd.agents;

import es.upm.emse.absd.Utils;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import lombok.extern.java.Log;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

@Log
public class FairyAgent extends Agent {

    protected void setup()
    {
        List<String> words = Arrays.asList("Hey!", "Listen!", "Hello!", "Link!");
        Random rand = new Random();

        Behaviour loop = new TiresomeBehaviour(words, rand);
        addBehaviour(loop);
    }

    private class TiresomeBehaviour extends TickerBehaviour {
        private final List<String> words;
        private final Random rand;

        public TiresomeBehaviour(List<String> words, Random rand) {
            super(FairyAgent.this, 2500);
            this.words = words;
            this.rand = rand;
        }

        protected void onTick() {
            ACLMessage msg = receive();
            if (msg != null) {
                log.info(this.getAgent().getLocalName() + ": HIIIIIIII!!! :D");
                doSuspend();
            }
            else {
                String randomWord = words.get(rand.nextInt(words.size()));
                log.info(this.getAgent().getLocalName() + ": " + randomWord);
                send(Utils.newMsg(this.getAgent(), ACLMessage.INFORM, randomWord, new AID("Link", AID.ISLOCALNAME)));
            }
        }

    }
}
