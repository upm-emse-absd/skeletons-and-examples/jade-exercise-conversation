package es.upm.emse.absd.agents;

import es.upm.emse.absd.Utils;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import lombok.extern.java.Log;

@Log
public class AdventurerAgent extends Agent {

    protected void setup()
    {

        CyclicBehaviour ce = new CyclicBehaviour() {
            @Override public void action() {
                ACLMessage msg = blockingReceive();
                if (msg.getContent().contains(this.getAgent().getLocalName())) {
                    log.info(this.getAgent().getLocalName() + ": WHAT!!!");
                    send(Utils.newMsg(this.getAgent(), ACLMessage.INFORM, "WHAT!!!", msg.getSender()));
                }
            }
        };

        addBehaviour(ce);

    }

}
