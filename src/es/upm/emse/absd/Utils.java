package es.upm.emse.absd;

import jade.core.AID;
import jade.core.Agent;
import jade.lang.acl.ACLMessage;

public final class Utils {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    private static int cidCnt = 0;
    private static String cidBase;

    public static String genCID(Agent agent)
    {
        if (cidBase==null) {
            cidBase = agent.getLocalName() + agent.hashCode() +
                System.currentTimeMillis()%10000 + "_";
        }
        return  cidBase + (cidCnt++);
    }

    public static ACLMessage newMsg(Agent origin, int perf, String content, AID dest)
    {
        ACLMessage msg = newMsg(origin, perf);
        if (dest != null) msg.addReceiver(dest);
        msg.setContent( content );
        return msg;
    }

    public static ACLMessage newMsg(Agent origin, int perf)
    {
        ACLMessage msg = new ACLMessage(perf);
        msg.setConversationId(genCID(origin));
        return msg;
    }

}
